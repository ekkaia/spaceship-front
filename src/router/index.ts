import { createRouter, createWebHistory } from "vue-router";
import Index from "@/views/Index.vue";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import Profile from "@/views/Profile.vue";
import Battle from "@/views/Battle.vue";
import Shop from "@/views/Shop.vue";
import GarageConfiguration from "@/views/GarageConfiguration.vue";
import Garage from "@/views/Garage.vue";
import { useAuth } from "@/stores/auth";
import { useMember } from "@/stores/member";
import Admin from "@/views/admin/Admin.vue";
import AdminMembers from "@/views/admin/AdminMembers.vue";
import AdminModuleEngine from "@/views/admin/modules/AdminModuleEngine.vue";
import AdminModuleShield from "@/views/admin/modules/AdminModuleShield.vue";
import AdminModuleSpaceship from "@/views/admin/modules/AdminModuleSpaceship.vue";
import AdminModuleCargo from "@/views/admin/modules/AdminModuleCargo.vue";
import AdminModuleWeapon from "@/views/admin/modules/AdminModuleWeapon.vue";

const router = createRouter({
	history: createWebHistory(),
	routes: [
		{
			path: "/",
			name: "index",
			component: Index,
		},
		{
			path: "/login",
			name: "login",
			component: Login,
		},
		{
			path: "/register",
			name: "register",
			component: Register,
		},
		{
			path: "/admin",
			name: "admin",
			component: Admin,
			meta: { requiresAuth: true, requiresAdmin: true },
		},
		{
			path: "/admin/members",
			name: "admin-members",
			component: AdminMembers,
			meta: { requiresAuth: true, requiresAdmin: true },
		},
		{
			path: "/admin/spaceshipModule",
			name: "admin-spaceshipModule",
			component: AdminModuleSpaceship,
			meta: { requiresAuth: true, requiresAdmin: true },
		},
		{
			path: "/admin/cargoModule",
			name: "admin-cargoModule",
			component: AdminModuleCargo,
			meta: { requiresAuth: true, requiresAdmin: true },
		},
		{
			path: "/admin/engineModule",
			name: "admin-engineModule",
			component: AdminModuleEngine,
			meta: { requiresAuth: true, requiresAdmin: true },
		},
		{
			path: "/admin/shieldModule",
			name: "admin-shieldModule",
			component: AdminModuleShield,
			meta: { requiresAuth: true, requiresAdmin: true },
		},
		{
			path: "/admin/weaponModule",
			name: "admin-weaponModule",
			component: AdminModuleWeapon,
			meta: { requiresAuth: true, requiresAdmin: true },
		},
		{
			path: "/members/me",
			name: "profile",
			component: Profile,
			meta: { requiresAuth: true },
		},
		{
			path: "/battle",
			name: "battle",
			component: Battle,
			meta: { requiresAuth: true },
		},
		{
			path: "/store",
			name: "shop",
			component: Shop,
			meta: { requiresAuth: true },
		},
		{
			path: "/garage",
			name: "garage",
			component: Garage,
			meta: { requiresAuth: true },
		},
		{
			path: "/garage/configuration/new",
			name: "garage-configuration-new",
			component: GarageConfiguration,
			meta: { requiresAuth: true },
		},
		{
			path: "/garage/configuration/:spaceshipId",
			name: "garage-configuration-id",
			component: GarageConfiguration,
			meta: { requiresAuth: true },
		},
	],
});

router.beforeResolve(async (to, _from) => {
	const authStore = useAuth();
	const memberStore = useMember();

	if (!authStore.isAuth && to.meta.requiresAuth) {
		return { name: "login" };
	}

	if (!memberStore.getMember.isAdmin && to.meta.requiresAdmin) {
		return { name: "profile" };
	}
});

export default router;
