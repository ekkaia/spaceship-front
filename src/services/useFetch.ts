import type { ResponseMessage } from "@/models/api.models";
import router from "../router/index";
import { useAuth } from "@/stores/auth";

export class useFetch {
	private readonly baseUrl: string = `${import.meta.env.VITE_PROTOCOL}://${import.meta.env.VITE_API_URL}/api`;
	private readonly router = router;
	private readonly token: string = localStorage.getItem("TOKEN") || "";

	public async get<ExpectedReturnType>(url: string, authRequired = true): Promise<ExpectedReturnType> {
		try {
			const options: RequestInit = {
				method: "GET",
			};
			if (authRequired) {
				options.headers = {
					"Authorization": `Bearer ${this.token}`,
				}
			}
			const res = await fetch(`${this.baseUrl}/${url}`, options);
			if (res.status !== 200) {
				if (res.status === 401) {
					this.unauthorizedHandler();
				}
				const error = await res.json();
				const response: ResponseMessage = {
					message: error.message,
					code: res.status
				};
				return Promise.reject(response);
			}
            const response: ExpectedReturnType = await res.json();
			return response;
		} catch (err) {
			throw err;
		}
	}

	public async post<ExpectedReturnType>(url: string, data: object, expectedResponseCode: 200 | 201, authRequired = true): Promise<ExpectedReturnType> {
		try {
			const options: RequestInit = {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(data),
			};
			if (authRequired) {
				options.headers = {
					...options.headers,
					"Authorization": `Bearer ${this.token}`,
				}
			}
			const res = await fetch(`${this.baseUrl}/${url}`, options);
			if (res.status !== expectedResponseCode) {
				if (res.status === 401) {
					this.unauthorizedHandler();
				}
				const error = await res.json();
				const response: ResponseMessage = {
					message: error.message,
					code: res.status
				};
				return Promise.reject(response);
			}
			const response: ExpectedReturnType = await res.json();
			return response;
		} catch (err) {
			throw err;
		}
	}

	public async put(url: string, data?: object, authRequired = true): Promise<any> {
		try {
			const options: RequestInit = {
				method: "PUT",
			};
			if (data) {
				options.body = JSON.stringify(data);
				options.headers = {
					"Content-Type": "application/json",
				};
			}
			if (authRequired) {
				options.headers = {
					...options.headers,
					"Authorization": `Bearer ${this.token}`,
				}
			}
			const res = await fetch(`${this.baseUrl}/${url}`, options);
			if (res.status !== 200) {
				if (res.status === 401) {
					this.unauthorizedHandler();
				}
				const error = await res.json();
				const response: ResponseMessage = {
					message: error.message,
					code: res.status
				};
				return Promise.reject(response);
			}
		} catch (err) {
			throw err;
		}
	}

	public async delete(url: string, authRequired = true): Promise<void> {
		try {
			const options: RequestInit = {
				method: "DELETE",
			};
			if (authRequired) {
				options.headers = {
					"Authorization": `Bearer ${this.token}`,
				}
			}
			const res = await fetch(`${this.baseUrl}/${url}`, options);
			if (res.status !== 204) {
				if (res.status === 401) {
					this.unauthorizedHandler();
				}
				const error = await res.json();
				const response: ResponseMessage = {
					message: error.message,
					code: res.status
				};
				return Promise.reject(response);
			}
		} catch (err) {
			throw err;
		}
	}

	private unauthorizedHandler() {
		localStorage.removeItem("TOKEN");
		const authStore = useAuth();
		authStore.isAuth = false;
		this.router.push({ path: "/login" });
	}
}
