import type { Configuration, ConfigurationLight } from "@/models/configuration.models";
import { useFetch } from "./useFetch";

export default {
	async getMainConfiguration(): Promise<Configuration> {
		try {
			const res = await new useFetch().get<Configuration>("members/me/configurations/main")
			return res;
		} catch (err) {
			throw err;
		}
	},
	async getConfigurations(): Promise<Configuration[]> {
		try {
			const res = await new useFetch().get<Configuration[]>("members/me/configurations")
			return res;
		} catch (err) {
			throw err;
		}
	},
	async updateMainConfiguration(spaceshipId: string): Promise<void> {
		try {
			await new useFetch().put(`members/me/configurations/${spaceshipId}/main`)
		} catch (err) {
			throw err;
		}
	},
	async deleteConfiguration(spaceshipId: string): Promise<void> {
		try {
			await new useFetch().delete(`members/me/configurations/${spaceshipId}`)
		} catch (err) {
			throw err;
		}
	},
	async createConfiguration(configuration: ConfigurationLight): Promise<void> {
		try {
			await new useFetch().post<void>("members/me/configurations", configuration, 201);
		} catch (err) {
			throw err;
		}
	},
}