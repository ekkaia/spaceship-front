import { useFetch } from "../useFetch";
import type { SpaceshipModel, UpdateSpaceshipModel } from "@/models/spaceship/spaceshipModel.models";

export default {
    async getSpaceshipModels(): Promise<SpaceshipModel[]> {
		try {
			const res = await new useFetch().get<SpaceshipModel[]>(`spaceship_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async buySpaceshipModel(spaceshipModelId: string): Promise<void> {
		try {
			const res = await new useFetch().get<void>(`spaceship_models/${spaceshipModelId}/buy`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async getOwnedSpaceshipModels(): Promise<SpaceshipModel[]> {
		try {
			const res = await new useFetch().get<SpaceshipModel[]>(`members/me/spaceship_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},

    async updateSpaceshipModel(dataSpaceship: UpdateSpaceshipModel, spaceshipModelId: string): Promise<UpdateSpaceshipModel>{
        try {
            const res = await new useFetch().put(`spaceship_models/${spaceshipModelId}`, dataSpaceship)
            return res;
        } catch (err) {
            throw err;
        }
    },

    async createSpaceshipModel(spaceshipModel: UpdateSpaceshipModel){
        try {
            const res = await new useFetch().post(`spaceship_models`, spaceshipModel, 201);
        } catch (err) {
            throw err;
        }
    },

	async deleteSpaceshipModel(spaceshipModelId: string){
        try {
            const res = await new useFetch().delete(`spaceship_models/${spaceshipModelId}`);
        } catch (err) {
            throw err;
        }
    }
}