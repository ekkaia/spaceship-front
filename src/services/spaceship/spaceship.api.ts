import type { Spaceship } from "@/models/spaceship/spaceship.models";
import { useFetch } from "../useFetch";

export default {
	async getOwnedSpaceships(): Promise<Spaceship[]> {
		try {
			const res = await new useFetch().get<Spaceship[]>(`members/me/spaceships`);
			return res;
		} catch (err) {
			throw err;
		}
	}
};