import type { Login, Register } from "@/models/auth.models";
import { useFetch } from "./useFetch";

export interface LoginResponse {
	token: string;
	expiration: string;
	message: string;
}

export default {
	async login(login: Login): Promise<LoginResponse> {
		try {
			const res = await new useFetch().post<LoginResponse>("auth/login", login, 200, false);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async register(register: Register): Promise<void> {
		try {
			await new useFetch().post<void>("auth/register", register, 201, false);
		} catch (err) {
			throw err;
		}
	},
	async getWebsocketToken(): Promise<LoginResponse> {
		try {
			const res = await new useFetch().get<LoginResponse>("auth/websocket_login");
			return res;
		} catch (err) {
			throw err;
		}
	}
};