import type { Member, MemberList,  UpdateMember } from "@/models/member.classes";
import { useFetch } from "./useFetch";

export default {
    async getMe(): Promise<Member> {
        try {
            const res = await new useFetch().get<Member>("members/me");
            return res;
        } catch (err) {
            throw err;
        }
    },

    async getMembers(limit: number, offset: number, search?: string): Promise<MemberList> {
        try {
            let url = `members?limit=${limit}&offset=${offset}`
            if (search) {
                url += `&search=${search}`;
            }
            const res = await new useFetch().get<MemberList>(url);
            return res;
        } catch (err) {
            throw err;
        }
    },

    async updateMember(dataMember: UpdateMember, memberId: string): Promise<UpdateMember>{
        try {
            const res = await new useFetch().put(`members/${memberId}`, dataMember)
            return res;
        } catch (err) {
            throw err;
        }
    },

    async deleteMember(memberId: string){
        try {
            await new useFetch().delete(`members/${memberId}`)
        } catch (err) {
            throw err;
        }
    }
};