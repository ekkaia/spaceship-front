import type { Cargo } from "@/models/cargo/cargo.models";
import { useFetch } from "../useFetch";

export default {
    async getOwnedCargos(): Promise<Cargo[]> {
        try {
            const res = await new useFetch().get<Cargo[]>(`members/me/cargos`);
			return res;
        } catch (err) {
            throw err;
        }
    },
};