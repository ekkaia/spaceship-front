import { useFetch } from "../useFetch";
import type { CargoModel, UpdateCargoModel } from "@/models/cargo/cargoModel.models";

export default {
    async getCargoModels(): Promise<CargoModel[]> {
		try {
			const res = await new useFetch().get<CargoModel[]>(`cargo_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async buyCargoModel(cargoModelId: string): Promise<void> {
		try {
			const res = await new useFetch().get<void>(`cargo_models/${cargoModelId}/buy`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async getOwnedCargoModels(): Promise<CargoModel[]> {
		try {
			const res = await new useFetch().get<CargoModel[]>(`members/me/cargo_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
    async updateCargoModel(cargoModelId: string, cargoModel: UpdateCargoModel): Promise<UpdateCargoModel> {
        try {
            const res = await new useFetch().put(`cargo_models/${cargoModelId}`, cargoModel);
            return res;
        
        } catch (err) {
            throw err;
        }
    },

    async createCargoModel(cargoModel: UpdateCargoModel){
        try {
            const res = await new useFetch().post(`cargo_models`, cargoModel, 201);
        } catch (err) {
            throw err;
        }
    },

	async deleteCargoModel(cargoModelId: string){
        try {
            const res = await new useFetch().delete(`cargo_models/${cargoModelId}`);
        } catch (err) {
            throw err;
        }
    }
}
