import { useFetch } from "../useFetch";
import type { EngineModel, UpdateEngineModel } from "@/models/engine/engineModel.models";

export default {
    async getEngineModels(): Promise<EngineModel[]> {
		try {
			const res = await new useFetch().get<EngineModel[]>(`engine_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async buyEngineModel(engineModelId: string): Promise<void> {
		try {
			const res = await new useFetch().get<void>(`engine_models/${engineModelId}/buy`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async getOwnedEngineModels(): Promise<EngineModel[]> {
		try {
			const res = await new useFetch().get<EngineModel[]>(`members/me/engine_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
    async updateEngineModel(engineModelId: string, engineModel: UpdateEngineModel): Promise<UpdateEngineModel> {
        try {
            const res = await new useFetch().put(`engine_models/${engineModelId}`, engineModel);
            return res;
        } catch (err) {
            throw err;
        }
    },
    async deleteEngineModel(engineModelId: string){
        try {
            const res = await new useFetch().delete(`engine_models/${engineModelId}`);
        } catch (err) {
            throw err;
        }
    },
    async createEngineModel(engineModel: UpdateEngineModel){
        try {
            const res = await new useFetch().post(`engine_models`, engineModel, 201);
        } catch (err) {
            throw err;
        }
    }
}


