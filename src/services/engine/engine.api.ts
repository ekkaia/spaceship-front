import type { Engine } from "@/models/engine/engine.models";
import { useFetch } from "../useFetch";

export default {
	async getOwnedEngines(): Promise<Engine[]> {
		try {
			const res = await new useFetch().get<Engine[]>(`members/me/engines`);
			return res;
		} catch (err) {
			throw err;
		}
	},
};