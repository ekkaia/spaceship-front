import type { Weapon } from "@/models/weapon/weapon.models";
import { useFetch } from "../useFetch";

export default {
    async getOwnedWeapons(): Promise<Weapon[]> {
        try {
            const res = await new useFetch().get<Weapon[]>(`members/me/weapons`);
            return res;
        } catch (err) {
            throw err;
        }
    },
};