import type { WeaponModel, UpdateWeaponModel} from "@/models/weapon/weaponModel.models";
import { useFetch } from "../useFetch";

export default {
    async getWeaponModels(): Promise<WeaponModel[]> {
		try {
			const res = await new useFetch().get<WeaponModel[]>(`weapon_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async buyWeaponModel(weaponModelId: string): Promise<void> {
		try {
			const res = await new useFetch().get<void>(`weapon_models/${weaponModelId}/buy`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async getOwnedWeaponModels(): Promise<WeaponModel[]> {
		try {
			const res = await new useFetch().get<WeaponModel[]>(`members/me/weapon_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
    async updateWeaponModel(weaponModelId: string, weaponModel: UpdateWeaponModel): Promise<UpdateWeaponModel> {
        try {
            const res = await new useFetch().put(`weapon_models/${weaponModelId}`, weaponModel);
            return res;
        } catch (err) {
            throw err;
        }
    },

    async createWeaponModel(weaponModel: UpdateWeaponModel){
        try {
            const res = await new useFetch().post(`weapon_models`, weaponModel, 201);
        } catch (err) {
            throw err;
        }
    },
	async deleteWeaponModel(weaponModelId: string){
        try {
            const res = await new useFetch().delete(`weapon_models/${weaponModelId}`);
        } catch (err) {
            throw err;
        }
    },
}

