import type { Shield } from "@/models/shield/shield.models";
import { useFetch } from "../useFetch";

export default {
    async getOwnedShields(): Promise<Shield[]> {
        try {
            const res = await new useFetch().get<Shield[]>(`members/me/shields`);
			return res;
        } catch (err) {
            throw err;
        }
    },
};