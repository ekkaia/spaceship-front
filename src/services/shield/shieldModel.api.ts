import { useFetch } from "../useFetch";
import type { ShieldModel, UpdateShieldModel } from "@/models/shield/shieldModel.models";

export default {
    async getShieldModels(): Promise<ShieldModel[]> {
		try {
			const res = await new useFetch().get<ShieldModel[]>(`shield_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async buyShieldModel(shieldModelId: string): Promise<void> {
		try {
			const res = await new useFetch().get<void>(`shield_models/${shieldModelId}/buy`);
			return res;
		} catch (err) {
			throw err;
		}
	},
	async getOwnedShieldModels(): Promise<ShieldModel[]> {
		try {
			const res = await new useFetch().get<ShieldModel[]>(`members/me/shield_models`);
			return res;
		} catch (err) {
			throw err;
		}
	},
    async updateShieldModel(shieldModelId: string, shieldModel: UpdateShieldModel): Promise<UpdateShieldModel> {
        try {
            const res = await new useFetch().put(`shield_models/${shieldModelId}`, shieldModel);
            return res;
        } catch (err) {
            throw err;
        }
    },

    async createShieldModel(shieldModel: UpdateShieldModel){
        try {
            const res = await new useFetch().post(`shield_models`, shieldModel, 201);
        } catch (err) {
            throw err;
        }
    },

	async deleteShieldModel(shieldModelId: string){
        try {
            const res = await new useFetch().delete(`shield_models/${shieldModelId}`);
        } catch (err) {
            throw err;
        }
    }
}
