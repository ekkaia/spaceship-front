import { defineStore } from "pinia";

const token = localStorage.getItem("TOKEN");
const expireAt = localStorage.getItem("EXPIRE_AT");

export const useAuth = defineStore({
	id: "auth",
	state: () => ({
		isAuth: !!token,
		expireAt,
	}),
});
