import type { Member } from "@/models/member.classes";
import { defineStore } from "pinia";

export const useMember = defineStore({
	id: "member",
	state: () => ({
		member: {
			memberId: "",
			name: "",
			email: "",
			credits: 0,
			isAdmin: false,
		} as Member,
	}),
	getters: {
		getMember(state): Member {
			return state.member;
		},
	}
});
