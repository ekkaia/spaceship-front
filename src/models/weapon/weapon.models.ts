export interface Weapon {
    weaponId: string;
	spaceshipId: string | null;
	weaponModelId: string;
	moduleSlotId: string | null;
	memberId: string;
	health: number;
	ammo: number;
	created_at: Date | null;
	updated_at: Date | null;
}