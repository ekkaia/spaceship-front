export enum WeaponType {
	Missile = "MISSILE",
	Gatling = "GATLING",
	Railgun = "RAILGUN",
}

export enum DamageType {
	Kinetic = "KINETIC",
	Electric = "ELECTRIC",
	Explosion = "EXPLOSION",
}

export enum AmmoType {
	HeavyAmmo = "HEAVY_AMMO",
	ElectricBulk = "ELECTRIC_BULK",
}

export interface WeaponModel {
	weaponModelId: string;
	name: string;
	weaponType: WeaponType;
	maxPower: number;
	maxAmmo: number;
	maxHealth: number;
	reloadTime: number;
	damage: number;
	damageType: DamageType;
	ammoType: AmmoType;
	travelTimeByDistance: number;
	minShootingDistance: number;
	maxShootingDistance: number;
	size: number;
	price: number;
}

export interface NewWeaponModel {
	name: string;
	weaponType: WeaponType;
	maxPower: number;
	maxAmmo: number;
	maxHealth: number;
	reloadTime: number;
	damage: number;
	damageType: DamageType;
	ammoType: AmmoType;
	travelTimeByDistance: number;
	minShootingDistance: number;
	maxShootingDistance: number;
	size: number;
	price: number;
}

export interface UpdateWeaponModel {
	name: string;
	weaponType: WeaponType;
	maxPower: number;
	maxAmmo: number;
	maxHealth: number;
	reloadTime: number;
	damage: number;
	damageType: DamageType;
	ammoType: AmmoType;
	travelTimeByDistance: number;
	minShootingDistance: number;
	maxShootingDistance: number;
	size: number;
	price: number;
}