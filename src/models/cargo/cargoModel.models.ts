export enum CargoType {
    Basic = "BASIC",
    RadiationProtection = "RADIATION_PROTECTION",
}

export interface NewCargoModel {
    name: string;
    maxHealth: number;
    cargoType: CargoType;
    capacity: number;
    size: number;
    price: number;
}

export interface CargoModel {
	cargoModelId: string;
	name: string;
	cargoType: CargoType;
	maxHealth: number;
	capacity: number;
	size: number;
	price: number;
}

export interface UpdateCargoModel {
    name: string;
    maxHealth: number;
    cargoType: CargoType;
    capacity: number;
    size: number;
    price: number;
}