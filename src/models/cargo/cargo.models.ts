export interface Cargo {
    cargoId: string;
    spaceshipId: string | null;
    cargoModelId: string;
    memberId: string;
    moduleSlotId: string | null;
    health: number;
    createdAt: Date | null;
    updatedAt: Date | null;
}