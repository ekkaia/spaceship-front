import type { CargoType } from "./cargo/cargoModel.models";

export interface ConfigurationLight {
	spaceshipId: string;
	spaceshipModelId: string;
	name: string;
	weapons: WeaponComponentLight[];
	engine: EngineComponentLight;
	shield: ShieldComponentLight;
	cargo: CargoComponentLight;
}

export interface WeaponComponentLight {
	weaponId: string | null;
	moduleSlotId: string;
}

export interface ShieldComponentLight {
	shieldId: string | null;
	moduleSlotId: string;
}

export interface CargoComponentLight {
	cargoId: string | null;
	moduleSlotId: string;
}

export interface EngineComponentLight {
	engineId: string;
	moduleSlotId: string;
}

export interface Configuration {
	spaceshipId: string;
	name: string;
	memberId: string;
	health: number;
	maxHealth: number;
	spaceshipModelId: string;
	modelName: string;
	spaceshipType: string;
	tankCapacity: number;
	maxPower: number;
	consumptionForPower: number;
	energyRepartitionDelay: number;
	isMainShip: boolean;
	moduleZones: ModuleZone[];
	moduleSlots: ModuleSlot[];
	engine: EngineComponent;
	weapons: WeaponComponent[];
	shield: ShieldComponent | null;
	cargo: CargoComponent | null;
}

export interface ModuleZone {
	shielding: number;
	moduleZoneId: string;
}

export interface ModuleSlot {
	moduleSlotId: string;
	moduleZoneId: string;
	moduleType: ModuleType;
	size: number;
}

export enum ModuleType {
	ENGINE = "ENGINE",
	WEAPON = "WEAPON",
	SHIELD = "SHIELD",
	CARGO = "CARGO",
}

export interface EngineComponent {
	engineId: string;
	moduleSlotId: string;
	name: string;
	health: number;
	maxHealth: number;
	maxPower: number;
	maxSpeed: number;
	engineModelId: string;
}

export interface CargoComponent {
	cargoId: string;
	cargoModelId: string;
	moduleSlotId: string;
	health: number;
	maxHealth: number;
	name: string;
	cargoType: CargoType;
	capacity: number;
}

export interface WeaponComponent {
	weaponId: string;
	weaponModelId: string;
	moduleSlotId: string;
	name: string;
	damage: number;
	health: number;
	maxHealth: number;
	magSize: number;
	ammoType: string;
	maxPower: number;
	reloadTime: number;
	nextShotTime: Date;
	damageType: string;
	weaponType: string;
	currentLoad: number;
	maxShootingDistance: number;
	minShootingDistance: number;
	travelTimeByDistance: number;
}

export interface ShieldComponent {
	name: string;
	moduleSlotId: string;
	health: number;
	maxPower: number;
	shieldId: string;
	shieldModelId: string;
	maxHealth: number;
	reloadTime: number;
	shieldType: string;
	shieldTypeProtection: number;
}