export enum EngineType {
    Basic = "BASIC",
}

export interface EngineModel {
	engineModelId: string;
	name: string;
	engineType: EngineType,
	maxHealth: number;
	maxPower: number;
	maxSpeed: number;
	size: number;
	price: number;
}

export interface NewEngineModel {
	name: string;
	maxHealth: number;
	maxPower: number;
	maxSpeed: number;
	engineType: EngineType,
	size: number;
	price: number;
}

export interface UpdateEngineModel {
	name: string;
	engineType: EngineType,
	maxHealth: number;
	maxPower: number;
	maxSpeed: number;
	size: number;
	price: number;
}