export interface Engine {
    engineId: string;
	spaceshipId: string | null;
	engineModelId: string;
	memberId: string;
	moduleSlotId: string | null;
	health: number;
	createdAt: Date | null;
	updatedAt: Date | null;
}