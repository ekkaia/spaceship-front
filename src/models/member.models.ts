import {
	Length,
	IsEmail,
	IsUUID,
	IsBoolean,
	IsNumber,
	IsPositive,
} from "class-validator";
import type { Metadata } from "./metadata.class";

export class Member {
	@IsUUID()
	memberId: string;

	@Length(4, 16)
	name: string;

	@IsEmail()
	email: string;

	@IsNumber()
	@IsPositive()
	credits: number;

	@IsBoolean()
	isAdmin: boolean;
}

export class UpdateMember {
	@Length(4, 16)
	name: string;

	@IsEmail()
	email: string;

	@IsNumber()
	@IsPositive()
	credits: number;

	@IsBoolean()
	isAdmin: boolean;
}

export class MemberList {
	members: Member[];
	metadata: Metadata;
}
