import type { Configuration } from "./configuration.models";

export interface BattleData {
    status: RoomState;
    roomId: string;
    opponentSpaceship: Configuration | null;
    spaceship: Configuration;
    opponentName: string;
    name: string;
}

export enum MessageType {
    JoinRoom = "JoinRoom",
    NewPlayerJoined = "NewPlayerJoined",
    OpponentData = "OpponentData",
    LeaveRoom = "LeaveRoom",
    OpponentStatusUpdate = "OpponentStatusUpdate",
    SpaceshipStatusUpdate = "SpaceshipStatusUpdate",
    AttackProcessingResult = "AttackProcessingResult",
}

export enum RoomState {
    Wait = "Wait",
    InGame = "InGame",
    End = "End",
    Refused = "Refused",
}

export interface JoinRoom {
    roomState: RoomState;
    roomId: string;
    messageType: MessageType.JoinRoom;
}

export interface NewPlayerJoined {
    messageType: MessageType.NewPlayerJoined;
    roomState: RoomState;
    playerName: string;
    playerSpaceship: Configuration;
}

export interface OpponentData {
    messageType: MessageType.OpponentData;
    name: string;
    spaceship: Configuration;
}

export interface LeaveRoom {
    messageType: MessageType.LeaveRoom;
    roomState: RoomState;
}

export interface Attack {
    weaponId: string;
}

export interface OpponentStatusUpdate {
    messageType: MessageType.OpponentStatusUpdate,
    health: number;
    hasWon: boolean;
    roomState: RoomState;
}

export interface SpaceshipStatusUpdate {
    messageType: MessageType.SpaceshipStatusUpdate,
    health: number;
    hasWon: boolean;
    roomState: RoomState;
}

export interface AttackProcessingResult {
    messageType: MessageType.AttackProcessingResult;
    ammos: number;
    nextShotTime: Date;
    weaponId: string;
}