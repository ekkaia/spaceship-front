import { IsEmail, Length, Matches } from "class-validator";

export class Login {
    @IsEmail()
    email: string;

    @Length(8, 32)
    @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
    password: string;
}

export class Register {
	@Length(4, 16)
    name: string;

    @IsEmail()
    email: string;

    @Length(8, 32)
    @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
    password: string;
}
