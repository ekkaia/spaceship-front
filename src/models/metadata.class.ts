import { IsNumber, IsPositive, IsOptional, IsString } from "class-validator";

export class Metadata {
    @IsNumber()
    @IsPositive()
    limit: number;

    @IsNumber()
    @IsPositive()
    offset: number;

    @IsOptional()
    @IsString()
    search?: string;
}