export interface Spaceship {
    spaceshipId: string;
	spaceshipModelId: string;
	name: string | null;
	memberId: string;
	health: number;
	isMainShip: boolean;
	createdAt: Date | null;
	updatedAt: Date | null;
}