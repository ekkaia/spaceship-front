import type { ModuleZone, ModuleSlot } from "../configuration.models";

export enum SpaceshipType {
	Basic = "BASIC",
	Fighter = "FIGTHER",
}

export interface SpaceshipModel {
	spaceshipModelId: string;
	name: string;
	maxHealth: number;
	spaceshipType: SpaceshipType;
	tankCapacity: number;
	maxPower: number;
	consumptionForPower: number;
	energyRepartitionDelay: number;
	price: number;
	moduleZones: ModuleZone[];
	moduleSlots: ModuleSlot[];
}

export interface NewSpaceshipModel {
	name: string;
	maxHealth: number;
	spaceshipType: SpaceshipType;
	tankCapacity: number;
	maxPower: number;
	consumptionForPower: number;
	energyRepartitionDelay: number;
	price: number;
}

export interface UpdateSpaceshipModel {
	name: string;
	maxHealth: number;
	spaceshipType: SpaceshipType;
	tankCapacity: number;
	maxPower: number;
	consumptionForPower: number;
	energyRepartitionDelay: number;
	price: number;
}