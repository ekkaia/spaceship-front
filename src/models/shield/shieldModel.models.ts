export enum ShieldType {
    Basic = "BASIC",
	Dispersion = "DISPERSION",
}

export interface ShieldModel {
	shieldModelId: string;
	name: string;
	shieldType: ShieldType;
	shieldTypeProtection: number;
	maxHealth: number;
	reloadTime: number;
	maxPower: number;
	size: number;
	price: number;
}

export interface NewShieldModel {
	name: string;
	shieldType: ShieldType,
	shieldTypeProtection: number;
	maxHealth: number;
	reloadTime: number;
	maxPower: number;
	size: number;
	price: number;
}

export interface UpdateShieldModel {
	name: string;
	shieldType: ShieldType;
	shieldTypeProtection: number;
	maxHealth: number;
	reloadTime: number;
	maxPower: number;
	size: number;
	price: number;
}