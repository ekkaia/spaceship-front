export interface Shield {
    shieldId: string;
	spaceshipId: string | null;
	shieldModelId: string;
	memberId: string;
	moduleSlotId: string | null;
	health: number;
	createdAt: Date | null;
	updatedAt: Date | null;
}